# Concurrent Programming Algorithm List

## Unit 2

- Concepts:

  - State
  - Atomic actions
  - "at-most-once"
  - History
  - Execution
  - Synchronization
  - Double-Check

- `<await(B);S;>`

- Demonstration of Concurrent Correctness

- P: Pattern matching inside a file

  - Sequential
  - Concurrent with variable inside the loop
  - Concurrent with Consumer/Producer (buffer size 1)

- P: Approximation of an integral

  - Sequential Iterative
  - Sequential Recursive
  - Concurrent Recursive

- P: Matrix multiplication with peers and coordinator

  - k peers (isolated), 1 coordinator
  - k peers (connected), 1 coordinator

- Consumer/Producer with `<await(B);S;>`, with invariants

- P: Given a concurrent program of simple arithmetic...

  - Tell how many possible histories there are
  - All the possible final values of the vars
  - Whether the program ends or not

- P: Using <> and `<await(B);S;>`, implement some simple program

## Unit 3

### Critical Section

- Formulation of the problem
- Protocol characteristics

  - Safety
  - Liveness

- P: Critical Section with `<await();>`:

  - For 2 processes
  - For n processes
  - Problems with the solution

- P: Critical Section with `test-and-set`

- P: Solve entry->lock problem:

  - Tiebreak

    - 2 processes
    - n processes

  - Ticket

    - Invariant
    - n processes with <> and `<await();>`
    - n processes without <> and `<await();>`:

      - with `fetch-and-add`
      - `<turn>` as a Critical Section

  - Bakery

    - for 2 processes
    - operator >>
    - for n processes

### Barriers

- Asymmetric Barriers

  - With Coordinator
  - Tree Barriers

- Symmetric Barriers

  - Butterfly barrier
  - Dissemination barrier

- P: Prefixes of an array

- Job Bag:

  - P: Matrix multiplication
  - P: Integral Approximation

## Semaphores

- P: Critical Section with Semaphores
- P: Barriers with Semaphores
- P: Bounded buffer with Semaphores

  - Buffer size 1, p producers, c consumers
  - Buffer size n, p producers, c consumers

- P: Dining Philosophers

- P: Readers/Writers problem

  - With Semaphores as Mutexes (elimination of Reader concurrency)
  - Conditional Synchronization
  - Pass of the baton

- P: Resource Planning

  - Pass of the baton
  - SJN

- Planners:

  - Unfair
  - Weakly fair
  - Strongly fair

## Monitors

- Semaphores using Monitors

  - Unfair
  - FIFO

- Interval timer

- Monitors using Semaphores

- Queues of a Monitor
