public class Server {
	ServerSocket s;
	HashTable<idU, infoUsr> tUsuarios;
	HashTable<idU, fout> tCanales;

	run() {
		initialize();
		while(true) {
			s = s.accept(); //Se bloquea hasta que entra otro cliente
			(new OyenteCliente(s)).start();
		}

	}
}

public class OyenteCliente {
	// Attributes:
	// 	- Socket
	// 	- fin
	// 	- fout

	// Process
	run() {
		while(true) {
			Mensaje m = (Mensaje) fin.readObject();
			switch(m.type) {
				case TipoMensaje.MENSAJE_CONEXION:
					tUsuarios.put(newID, newInfo); // Store new user in table
					tCanales.put(newID, newOutputStream); // Store the new channel
					// Send confirmation message
					break;
				case TipoMensaje.MENSAJE_LISTA_USUARIOS:
					// Retrieve info in tUsuarios
					// Send message MENSAJE_CONFIRMACION_LISTA_USUARIOS
					break;
				case TipoMensaje.MENSAJE_EMITIR_FICHERO:
					// Retrieve info of the user userWithFile with the file
					// Send message MENSAJE_PETICION_FICHERO to userWithFile
					break;
				case TipoMensaje.MENSAJE_PREPARA_COMUNICACION_CLIENTE_SERVIDOR:
					// Retrieve info of the user appropriateUser target for connection
					// Send message PREPARA_COMUNICACION_CLIENTE_SERVIDOR to appropriateUser
					break;
			}
		}
	}

}

public class Cliente {
	// Attributes:
	//	- IPServer
	//	- PortServer
	//	- Socket
	// 	- fout (output)
	// 	- fin (input)
	//	- puertoP2P
	// 	- myIP

	// Load name and info
	// Create socket
	// Send message: MENSAJE_CONEXION
	// Create a new thread OyenteServidor
	// Show menu to the user:
	// 	[-] Ask for available user list
	// 	[-] Ask for files
	// 	[-] Close connection

}

public class OyenteServidor {
	// Attributes:
	// 	- Socket
	// 	- fin
	// 	- fout

	run() {
		while (true) {
			Mensaje m = (Mensaje) fin.readObject();
			switch(m.type) {
				case TipoMensaje.MENSAJE_CONFIRMACION_CONEXION:

					break;
				case TipoMensaje.MENSAJE_CONFIRMACION_LISTA_USUARIOS:
					break;
				case TipoMensaje.MENSAJE_PETICION_FICHERO:
					break;
				case TipoMensaje.MENSAJE_PREPARA_COMUNICACION_SERVIDOR_CLIENTE:
					break;
			}
		}
	}


}

public class EmisorFicheros extends Thread{ // Thread started by OynteServidor
	// Crear serversocket

	// accept
	// crear flujoservidor
	// escribir fichero en el output

}

public class ReceptorFichero extends Thread {
	// Crear socket con servidorp2p
	// Crear flujo entrada
	// leer string
}
