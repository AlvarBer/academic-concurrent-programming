package lib.parser;

import java.util.*;

public class ConsoleArgumentParser {

    private List<String> acceptedArguments;

    public ConsoleArgumentParser(String[] acceptedArguments) {
        this.acceptedArguments = new ArrayList<String>();
        Collections.addAll(this.acceptedArguments, acceptedArguments);
    }

    public Map<String, String> parseArguments (String[] args) {
        Map<String, String> arguments = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            String command = args[i];
            if (this.acceptedArguments.contains(commandName(command)))
                arguments.put(commandName(command), commandValue(command));
        }

        return arguments;
    }

    private String commandName(String command) {
        if (command.contains("="))
            return command.substring(0, command.indexOf('='));

        return command;
    }

    private String commandValue(String command) {
        if (!command.contains("="))
            return "";

        return command.substring(command.indexOf("=") + 1, command.length());
    }

}
