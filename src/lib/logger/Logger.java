package lib.logger;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Logger {

    private StringBuilder s;
    private String name;
    private String path;

    private static final String DIR = "./lab10/logs/";

    public Logger(String name) {
        s = new StringBuilder();
        this.name = name;
        this.path = DIR + name + ".md";

        s.append("# " + name + "\n\n");
    }

    public void dump() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(path);

            writer.append(s.toString());

            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void logSubtitle(String subtitle) {
        s.append("## " + subtitle + "\n\n");
    }

    public void log(String message) {
        s.append(message + "\n\n");
    }

    public void log(String header, String message) {
        s.append("### " + header + "\n\n");
        s.append(message + "\n");
    }

    public void log(Object header, Object o) {
        this.log(header.toString(), o.toString());
    }

}
