package common.lab1.part1;
import java.util.ArrayList;

/**
 * @author usuario_local
 *
 */
public class Main implements Runnable {

	private long n;
	private long t;
	
	public Main(long n, long t) {
		this.n = n;
		this.t = t;
	}	
	
	@Override
	public void run() {
		System.out.println("My ID is: " + n);
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) {
		int n = 5;		
		long t = 1000;
		ArrayList<Thread> threads = new ArrayList<Thread>();
		for (long i = 0; i < n; i++) {
			threads.add(new Thread(new Main(i, t)));
		}
		
		for (Thread thr : threads) {
			thr.start();			
		}
		
		for (Thread thr : threads) {
			try {
				thr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("All threads ended");
		
		
		
	}

}
