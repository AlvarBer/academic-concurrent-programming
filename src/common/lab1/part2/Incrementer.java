package common.lab1.part2;

public class Incrementer implements Runnable {

	@Override
	public void run() {
		for(int i = 0; i < Main.N; ++i){
			Main.n++;
		}
		System.out.println("Inc n=" + Main.n);
	}
}