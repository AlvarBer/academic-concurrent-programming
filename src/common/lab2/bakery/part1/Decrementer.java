package common.lab2.bakery.part1;

public class Decrementer implements Runnable {

	@Override
	public void run() {
		for(int i = 0; i < Main.N; ++i){
			// Entry
			Main.t2 = 1;
			Main.t2 = Main.t1 + 1; // Take turn
			while(Main.t1 != 0 && Main.t2 > Main.t1); // Active wait
						
			//System.out.println("Dec n=" + Main.n + "  t1=" + Main.t1 + " t2=" + Main.t2);
			Main.n--; // CS
			
			// Exit
			Main.t2 = 0; // Notify that we are in NO_CS
		}	
		System.out.println("Dec n=" + Main.n);
	}
	
}