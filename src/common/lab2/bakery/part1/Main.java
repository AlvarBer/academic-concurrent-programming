package common.lab2.bakery.part1;

import java.util.ArrayList;


public class Main {
	
	//Attributes
	static final protected long N = 1000;
	static final private long M = 3000;
	static volatile int n = 0;
	static volatile int t1 = 1;
	static volatile int t2 = 1;
	
	public static void main(String[] args) {
		
		for (int i = 0; i < 10; i++) {
			ArrayList<Thread> threads = new ArrayList<Thread>();
			
			System.out.println("=== Start Execution ===");
			
			//for (long i = 0; i < M; i++) {
				threads.add(new Thread(new Incrementer()));
				threads.add(new Thread(new Decrementer()));			
			//}
			
			for (Thread thr : threads) {
				thr.start();			
			}
			
			try {
				for (Thread thr : threads) {			
						thr.join();			
				}
			} catch (InterruptedException aids) {
				aids.printStackTrace();
			}
			
			System.out.println("Final n=" + Main.n);
		}
		
	}

}