package common.lab2.bakery.part1;

public class Incrementer implements Runnable {

	@Override
	public void run() {
		for(int i = 0; i < Main.N; ++i){			
			// Entry
			Main.t1 = 1;
			Main.t1 = Main.t2 + 1; // Take turn
			while(Main.t2 != 0 && Main.t1 >= Main.t2); // Active wait
			
			//System.out.println("Dec n=" + Main.n + "  t1=" + Main.t1 + " t2=" + Main.t2);
			Main.n++; // CS
			
			// Exit
			Main.t1 = 0; // Notify that we are in NO_CS			
		}
		System.out.println("Inc n=" + Main.n);
	}
}