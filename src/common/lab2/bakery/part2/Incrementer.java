package common.lab2.bakery.part2;

import java.util.Collections;

public class Incrementer implements Runnable {

	private int id;
	
	public Incrementer(int id) {
		super();
		this.id = id;
	}
	
	@Override
	public void run() {
		for(int i = 0; i < Main.N; ++i){			
			// Entry
			Main.turns.set(id, 1);
			Main.turns.set(id, Collections.max(Main.turns) + 1); // Take turn
			for (int j = 1; j < Main.turns.size(); j++) {
				if (j != i) 
					while(Main.turns.get(j) != 0 && Main.angleAngle(Main.turns.get(i), i, Main.turns.get(j), j)); // Active wait
			}
			
			//System.out.println("Dec n=" + Main.n );
			Main.n++; // CS
			
			// Exit
			Main.turns.set(id, 0); // Notify that we are in NO_CS	
		}
		System.out.println("Inc n=" + Main.n);
	}
}