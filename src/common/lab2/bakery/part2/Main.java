package common.lab2.bakery.part2;

import java.util.ArrayList;

public class Main {
	
	//Attributes
	static final protected int N = 10;
	static final private int M = 30;
	static volatile int n = 0;
	static volatile ArrayList<Integer> turns;
	
	public static void main(String[] args) {
		
		turns = new ArrayList<Integer>(2*M);
		for (int i = 0; i < 2 * M; i++) {
			turns.add(1);
		}
		
		ArrayList<Thread> threads = new ArrayList<Thread>();
		
		System.out.println("=== Start Execution ===");
		
		for (int i = 0; i < M; i++) {
			threads.add(new Thread(new Incrementer(i)));
			threads.add(new Thread(new Decrementer(M+i)));			
		}
		
		for (Thread thr : threads) {
			thr.start();			
		}
		
		try {
			for (Thread thr : threads) {			
					thr.join();			
			}
		} catch (InterruptedException aids) {
			aids.printStackTrace();
		}
		
		System.out.println("Final n=" + Main.n);	
		
	}
	
	public static boolean angleAngle(int a, int b, int c, int d) {
		return a > c || (a == c && b > d);
	}

}