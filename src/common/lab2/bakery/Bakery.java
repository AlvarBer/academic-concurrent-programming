package common.lab2.bakery;

import common.lab2.bakery.part2.Main;

import java.util.Collections;
import java.util.List;

/**
 * Created by mortadelegle on 17/03/16.
 */
public class Bakery {
	public void bakeEnter(List<Integer> turnList, int id, int otherId) {
		turnList.set(id, 1);
		turnList.set(id, Collections.max(turnList) + 1); // Take turn
		for (int j = 1; j < turnList.size(); j++) {
			if (j != otherId)
				while(turnList.get(j) != 0 && Main.angleAngle(turnList.get(otherId), otherId, turnList.get(j), j)); // Active wait
		}
	}

	public void bakeExit(List<Integer> turnList, int id) {
		turnList.set(id, 0); // Notify that we are in NO_CS
	}
}
