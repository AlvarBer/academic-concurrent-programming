package lab5.server;

import lab5.common.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;

public class ClientListener implements Runnable {
    // Atributes
    private String name;
    private InetAddress ip;
    private BufferedReader read;
    private PrintWriter write;

	// Constructor
    public ClientListener(BufferedReader buf, PrintWriter wri){
        read = buf;
	    write = wri;
    }

	@Override
	public void run() {
		try {
			String str = read.readLine();
			System.out.println(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void handleMessages(Message message) {
		Message.MessageEnum m = message.getType();
		switch (m) {
			case UserList:
				sendUserList(message.getOrigin(), message.getDestiny());
				break;
			case RequestFile:
				requestFile();
				break;
		}

	}

	private void sendUserList(String Origin, String Destiny) {

	}

	private void requestFile() {

	}

}
