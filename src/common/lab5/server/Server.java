package lab5.server;

import lab5.common.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    // Attributes
    private BufferedReader read;
    private PrintWriter write;
    private User users[];

	// Constructors
	public Server() {
		listen();
	}

    // Methods
    public void sendHello() {
        write.println("Hello Client! I'm the server");
    }

    public void listen() {
	    try {
		    ServerSocket s = new ServerSocket(5556);
		    Socket sckt;

		    while(true) {
			    sckt = s.accept();
			    read = new BufferedReader(new InputStreamReader(sckt.getInputStream()));

			    write = new PrintWriter(new OutputStreamWriter(sckt.getOutputStream()));
			    System.out.println("Connection established");
			    new Thread(new ClientListener(read, write)).start();
		    }
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
    }
}
