package lab5.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    // Attributes
    private BufferedReader read;
    private PrintWriter write;
    private String name;
    private InetAddress ip;


    public void sendHello() {
        write.println("Hello Server! I'm the client");
        write.flush();
    }

	public Client() {
        try {
            Socket sckt = new Socket("localhost", 5556);
            read = new BufferedReader(new InputStreamReader(sckt.getInputStream()));
            write = new PrintWriter(new OutputStreamWriter(sckt.getOutputStream()));
	        new Thread(new ServerListener(read)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
