package lab5.client;

import lab5.common.Message;

import java.io.*;

public class ServerListener implements Runnable {
    private BufferedReader read;

	public ServerListener(BufferedReader buf) {
		read = buf;
	}

    @Override
    public void run() {
	    try {
		    String str = read.readLine();
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
    }

	private void handleMessages(Message m) {
		Message.MessageEnum message = m.getType();
		switch (message) {
			case GiveUserList:
				break;
		}
	}
}
