package lab5.common;

import lab5.client.Client;
import lab5.server.Server;

/**
 * Created by mortadelegle on 12/05/16.
 */
public class MainClient {
    public static void main(String[] args) {
	    System.out.println("Main started");
        Client cli = new Client();
	    System.out.println("Client started");
        cli.sendHello();
        System.out.println("Hello sent");
    }
}
