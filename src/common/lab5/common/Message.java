package lab5.common;

/**
 * Created by mortadelegle on 12/05/16.
 */
public abstract class Message {

	public enum MessageEnum {
		UserList, RequestFile, GiveUserList, EmitFile
	}

	public String origin;
	public String destiny;
	public MessageEnum type;

	public abstract MessageEnum getType();
	public abstract String getOrigin();
	public abstract String getDestiny();

}
