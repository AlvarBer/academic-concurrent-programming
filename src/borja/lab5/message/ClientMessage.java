package borja.lab5.message;

import borja.lab5.UserInfo;

public class ClientMessage extends Message {
    public ClientMessage(UserInfo senderInfo, MessageType type) {
        super(type, "Request Connection", senderInfo);
    }
}
