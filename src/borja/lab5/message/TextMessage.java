package borja.lab5.message;

import borja.lab5.UserInfo;

public class TextMessage extends Message {
    public TextMessage(String msg, UserInfo info) {
        super(MessageType.SEND_TEST_MESSAGE, msg, info);
    }
}
