package borja.lab5.message;

import borja.lab5.UserInfo;

import java.io.Serializable;

public abstract class Message implements Serializable{

    private String body;
    private MessageType type;
    private UserInfo sender;

    public Message(MessageType type, String msg, UserInfo senderInfo) {
        this.type = type;
        this.body = msg;
        this.sender = senderInfo;
    }

    public MessageType getMessageType() {
        return type;
    }

    public String getSender() {
        return sender.getName();
    }

    @Override
    public String toString() {
        return body;
    }

    public UserInfo getSenderInfo() {
        return sender;
    }
}
