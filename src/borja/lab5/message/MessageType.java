package borja.lab5.message;

import java.io.Serializable;

public class MessageType implements Serializable {

    private static String[] allTypes = new String[2];

    private int id;
    private String name;

    public static final MessageType SEND_TEXT_MESSAGE = new MessageType(0, "Send Text Message");
    public static final MessageType ESTABLISH_CONNECTION = new MessageType(1, "Establish Connection");

    public MessageType(int id, String name) {
        this.id = id;
        this.name = name;

        allTypes[id] = name;
    }

    public static String[] getPossibleValues() {
        return allTypes;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageType that = (MessageType) o;

        if (id != that.id) return false;
        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }

    public static MessageType fromName(String messageName) {

        boolean found = false;
        int i = 0;
        while (i < allTypes.length && !found) {
            if (allTypes[i].equals(messageName))
                found = true;
            else
                i++;
        }

        return fromID(i);
    }

    private static MessageType fromID(int id) {
        switch (id) {
            case 0:
                return MessageType.SEND_TEST_MESSAGE;
            case 1:
                return MessageType.ESTABLISH_CONNECTION;
        }
        return null;
    }
}
