package borja.lab5;

import borja.lab5.message.Message;
import borja.lab5.message.MessageType;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import static borja.lab5.message.MessageType.ESTABLISH_CONNECTION;
import static borja.lab5.message.MessageType.SEND_TEST_MESSAGE;

public class Server extends Thread{

    public static final String IPServer = "127.0.0.1";
    public static final int PortServer = 5556;

    ServerSocket listen;
    private Map<String, UserInfo> users;
    private Map<String, ObjectOutputStream> channels;

    @Override
    public void run() {
        initializeServer();

        while(true) {
            try {
                Socket socket = listen.accept();
                (new ClientListener(socket)).start();
            } catch (Exception ignored) {}
        }
    }

    private void initializeServer() {
        users = new HashMap<>();
        channels = new HashMap<>();

        try {
            listen = new ServerSocket(PortServer);
            System.out.println("Server started.");
            System.out.println("Listening on " + Server.IPServer + ":" + Server.PortServer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ClientListener extends Thread{

        private Socket socket;
        private ObjectInputStream readStream;
        private ObjectOutputStream writeStream;

        public ClientListener(Socket socket) throws IOException {
            this.socket = socket;
            this.writeStream = new ObjectOutputStream(socket.getOutputStream());
            this.readStream = new ObjectInputStream(socket.getInputStream());

            System.out.println("Client connected to socket: " + socket);
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Message m = (Message) readStream.readObject();
                    handleMessage(m);
                } catch (Exception ignored) {}
            }
        }

        private void handleMessage(Message m) {
            MessageType messageType = m.getMessageType();
            if (messageType.equals(MessageType.ESTABLISH_CONNECTION)) {
                users.put(m.getSender(), m.getSenderInfo());
                System.out.println("Added new user information for user" + m.getSenderInfo());

            }
            System.out.println(m);
        }
    }
}
