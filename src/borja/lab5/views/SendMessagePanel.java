package borja.lab5.views;

import borja.lab5.Client;
import borja.lab5.message.MessageType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class SendMessagePanel extends JPanel {

    Client client;

    MessageType messageType;

    public SendMessagePanel(Client c) {
        client = c;

        this.setLayout(new FlowLayout());

        initializeGUIComponents();
    }

    private void initializeGUIComponents() {
        JTextField messageTexField = new JTextField();
        messageTexField.setPreferredSize(new Dimension(200, 30));

        JComboBox<String> messageTypeComboBox = new JComboBox<String>(MessageType.getPossibleValues());
        messageTypeComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                String messageName = (String)cb.getSelectedItem();
                updateMessageType(messageName);
            }
        });
        messageTypeComboBox.setSelectedIndex(0);
        updateMessageType(MessageType.SEND_TEST_MESSAGE.getName());

        JButton sendButton = new JButton("Send");
        sendButton.addActionListener(e -> {
            try {
                client.sendMessage(messageTexField.getText(), messageType);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            messageTexField.setText("");
        });

        this.add(messageTexField);
        this.add(sendButton);
        this.add(messageTypeComboBox);
    }

    private void updateMessageType(String messageName) {
        messageType = MessageType.fromName(messageName);
    }


    private class MessageTypeComboBox extends JComboBox {
        public MessageTypeComboBox() {
        }
    }
}
