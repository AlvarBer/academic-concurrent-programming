package borja.lab5.views;

import borja.lab5.Client;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class ClientWindow extends JFrame{

    private Client client;

    public ClientWindow(String name, int id) {
        super(name);
        this.getContentPane().setLayout(new FlowLayout());

        initializeClient(id, name);
        initializeGUIComponents();

        this.pack();
        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void initializeGUIComponents() {

        JPanel connectPanel = new ConnectToServerPanel(client);
        JPanel sendMessagePanel = new SendMessagePanel(client);

        this.add(connectPanel);
        this.add(sendMessagePanel);
    }

    private void initializeClient(int clientId, String clientName) {
        client = new Client(clientId, clientName);
    }
}
