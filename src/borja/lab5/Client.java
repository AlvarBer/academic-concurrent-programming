package borja.lab5;

import borja.lab5.message.ClientMessage;
import borja.lab5.message.MessageType;
import borja.lab5.message.TextMessage;

import java.io.*;
import java.net.Socket;

import static borja.lab5.message.MessageType.ESTABLISH_CONNECTION;
import static borja.lab5.message.MessageType.SEND_TEST_MESSAGE;

public class Client extends Thread{

    private String serverIP;
    private int serverPort;

    Socket socket;
    ObjectInputStream readChannel;
    ObjectOutputStream writeChannel;
    UserInfo userInfo;

    public Client(int id, String username) {
        this.userInfo = new UserInfo(id, username);
    }

    @Override
    public void run() {
        connectToServer();
    }

    public void connectToServer() {
        System.out.println("Attempting Connection...");
        try {
            socket = new Socket(serverIP, serverPort);
            writeChannel = new ObjectOutputStream(socket.getOutputStream());
            readChannel = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message, MessageType type) throws IOException {
        if (type == ESTABLISH_CONNECTION) {
            sendClientMessage(type);
        } else {
            sendTextMessage(message);
        }
    }

    public void sendTextMessage(String message) throws IOException {
        writeChannel.writeObject(new TextMessage(userInfo + " says: " + message, userInfo));
        writeChannel.flush();
    }

    public void sendClientMessage(MessageType type) throws IOException {
        writeChannel.writeObject(new ClientMessage(userInfo, type));
        writeChannel.flush();
    }

    public void setServerContext(String serverIP, int serverPort) {
        this.serverIP = serverIP;
        this.serverPort = serverPort;
    }
}
