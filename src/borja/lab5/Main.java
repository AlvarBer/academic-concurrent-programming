package borja.lab5;

import borja.lab5.views.ClientWindow;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        ClientWindow client1 = new ClientWindow("Client", 0);
        ClientWindow client2 = new ClientWindow("Client", 1);

        server.start();
    }

}
