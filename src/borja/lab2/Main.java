package borja.lab2;


import borja.lab2.locks.BakeryLock;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final int N = 1;
    public static final int M = 4;

    public static int n = 0;

    public static final LockTypes lockType = LockTypes.BAKERY;

    public static void main(String[] args) throws InterruptedException {

        List<Operator> threads = new ArrayList<>();
        BakeryLock.init();

        for (int i = 0; i < M; i++) {
            threads.add(new Incrementer(i));
            threads.add(new Decrementer(i+M));
        }

        threads.forEach(Thread::start);

        for (Thread t : threads) {
            t.join();
        }

        System.out.println("\nAll said and done, N = " + Main.n);

    }

}

