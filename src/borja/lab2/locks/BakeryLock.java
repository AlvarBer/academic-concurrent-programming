package borja.lab2.locks;

import borja.lab2.Main;

public class BakeryLock {

    public static int[] turns;

    public static void init() {
        int[] turns = new int[2* Main.M ];
        for (int i = 0; i < turns.length; i++) {
            turns[i] = 1;
        }
        BakeryLock.turns = turns.clone();
    }

    public static int getNextTurn() {
        int maxTurn = 0;

        for (int turn : BakeryLock.turns) {
            if (turn > maxTurn) {
                maxTurn = turn;
            }
        }

        return maxTurn + 1;
    }

    public static boolean compare(int turna, int ida, int turnb, int idb) {
        // the thread IDs act as tiebreakers
        return (turna > turnb) || ((turna == turnb) && ida > idb);
    }

    public static String turnsString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int turn : BakeryLock.turns) {
            sb.append(turn+", ");
        }
        sb.append("]");

        return sb.toString();
    }

    public static void wait(int id) {
        BakeryLock.turns[id] = 1; // We are awake
        BakeryLock.turns[id] = BakeryLock.getNextTurn(); // Take turn
        for (int turn = 0; turn < BakeryLock.turns.length; turn++) {
            // Wait for your turn.
            // Once we wait for turns[turn], it can only go later than us
            // It can't take a turn higher than us
            while (
                    BakeryLock.turns[turn] != 0 &&
                    BakeryLock.compare(
                            BakeryLock.turns[id], id,
                            BakeryLock.turns[turn], turn)) ;
        }
    }

    public static void signal(int id) {
        BakeryLock.turns[id] = 0;
    }

}
