package borja.lab2.locks;

import borja.lab2.Main;

import java.util.ArrayList;
import java.util.List;

public class TieBreakLock {

    // steps[id] indicates which step the thread "id" is on
    private static List<Integer> steps;

    // last[i] indicates the last thread to be in step i,
    // so that if a thread sees that no thread has gone after him,
    // and returns to the same step, it waits for another one.
    private static List<Integer> last;


    public static void init() {
        steps = new ArrayList<>();
        last = new ArrayList<>();
        for (int i = 0; i < 2*Main.M; i++) {
            steps.add(0);
            last.add(0);
        }
    }

    public static void wait(int id){
        // Before I'm alowed to continue, I must
        // traverse all the steps.
        //
        // For each step, I have to...
        for (int step = 0; step < steps.size(); step++) {
            steps.set(id, step); // ...arrive at the step
            last.set(step, id); // ...and thus I am the last to arrive
            for (int k = 0; k < steps.size(); k++) { // ... but I have to make sure that
                                                    // ... for each other thread
                while (k != id && // ... I don't continue
                        last.get(step) == id && // ... while I'm the last to arrive at the step
                        stepOf(k) >= stepOf(id)); // ... and my step is smaller than everybody else's
                // In other words, I don't continue until my step is the most advanced,
                // and I'm not the last one to be there
            }
        }
    }

    public static void signal(int id) {

    }

    private static Integer stepOf(int id) {
        return steps.get(id);
    }

}
