package borja.lab2;

import borja.lab2.locks.BakeryLock;

public abstract class Operator extends Thread {

    protected int id;

    public Operator(int id) {
        super();
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("Thread [" + id + "]: I'm live!");

        for (int operationCount = 0; operationCount < Main.N; operationCount++) {

            // Enter
            BakeryLock.wait(id);

            // CS
            System.out.println("Thread [" + id + "]: Entering the CS");
            Main.n = this.operation();
            System.out.println("Thread [" + id + "]: Exiting the CS");

            // Exit
            BakeryLock.signal(id);
        }

        System.out.println("Thread [" + id + "]: I'm out. N = " + borja.lab1.part2.Main.n);

    }

    protected abstract int operation();

}
