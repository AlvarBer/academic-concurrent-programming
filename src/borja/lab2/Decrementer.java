package borja.lab2;

import borja.lab1.part2.Main;

public class Decrementer extends Operator {

    public Decrementer(int id) {
        super(id);
    }

    @Override
    protected int operation() {
        return Main.n - 1;
    }
}
