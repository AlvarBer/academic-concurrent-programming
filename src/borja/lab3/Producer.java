package borja.lab3;

import borja.lab1.part2.Operator;

public class Producer extends OperatorSemaphore{

    public Producer(String id, Storage storage) {
        super(id, storage);
    }

    @Override
    protected void operate() throws InterruptedException {
        produce();
    }


    private void produce() throws InterruptedException {
        Product product = new Product("Created by: " + id);
        System.out.println("[" + id + "] is waiting to store...");
        storage.store(product);
        System.out.println("[" + id + "] stored, size is now " + storage.getNumberOfStoredItems());
    }
}
