package borja.lab3;

public class Consumer extends OperatorSemaphore {

    public Consumer(String id, Storage storage) {
        super(id, storage);
    }

    @Override
    protected void operate() throws InterruptedException {
        consume();
    }

    private void consume() throws InterruptedException {
        System.out.println("[" + id + "] is waiting to consume...");
        Product consumedProduct = storage.acquire();
        System.out.println("[" + id + "] consumed: "+consumedProduct.toString());
    }
}
