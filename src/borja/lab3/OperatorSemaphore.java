package borja.lab3;

abstract class OperatorSemaphore extends Thread {
    protected String id;
    Storage storage;

    public OperatorSemaphore(String id, Storage storage) {
        this.id = id;
        this.storage = storage;
    }

    @Override
    public void run() {

        while(true) {
            try {
                operate();
                //Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected abstract void operate() throws InterruptedException;

}
