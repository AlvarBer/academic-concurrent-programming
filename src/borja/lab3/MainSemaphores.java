package borja.lab3;

import javafx.beans.binding.IntegerBinding;
import lib.parser.ConsoleArgumentParser;

import java.util.ArrayList;
import java.util.Map;

public class MainSemaphores {

    private static final String[] ARGUMENTS = {"-p", "-c", "-s"};
    private static Map<String, String> arguments;

    public static void main(String[] args) {

        ConsoleArgumentParser parser = new ConsoleArgumentParser(ARGUMENTS);
        arguments = parser.parseArguments(args);
        ArrayList<OperatorSemaphore> operators = new ArrayList<>();

        int storageSize = getValueOfArgument("-s");
        int numberOfProducers = getValueOfArgument("-p");
        int numberOfConsumers = getValueOfArgument("-c");

        Storage storage = new Storage(storageSize);

        for (int i = 0; i < numberOfProducers; i++) {
            operators.add(new Producer("Producer "+i, storage));
        }
        for (int i = 0; i < numberOfConsumers; i++) {
            operators.add(new Consumer("Consumer "+i, storage));
        }

        for (OperatorSemaphore operator : operators) {
            operator.start();
        }

        for (OperatorSemaphore operator : operators) {
            try {
                operator.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static int getValueOfArgument(String argument) {
        int result = 0;
        try {
            result = Integer.parseInt(arguments.get(argument));
        } catch (NumberFormatException e) {     }
        return  result;
    }
}
