package borja.lab3;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Semaphore;

public class Storage {

    private Semaphore full;
    private Semaphore empty;
    private Semaphore mutex;
    private Deque<Product> products;
    private int maxProducts;

    public Storage(int maxProducts) {
        mutex = new Semaphore(1);
        empty = new Semaphore(0);
        full = new Semaphore(maxProducts);
        products = new ArrayDeque<>(maxProducts);
        this.maxProducts = maxProducts;
    }

    public void store(Product product) throws InterruptedException {
        full.acquire(); // Wait until not full
        mutex.acquire();
        products.addLast(product);
        mutex.release();
        empty.release(); // Not empty anymore
    }

    public Product acquire() throws InterruptedException {
        Product result;
        empty.acquire(); // Wait until empty
        mutex.acquire();
        result = products.removeFirst();
        mutex.release();
        full.release(); // Not full anymore
        return result;
    }

    public int getNumberOfStoredItems() {
        return products.size();
    }
}
