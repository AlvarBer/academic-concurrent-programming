package borja.lab1.part1;


import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final int N = 3;
    public static final int T = 2000;

    public static void main(String[] args) throws InterruptedException {

        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            threads.add(new SimpleThread(i));
        }

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            t.join();
        }

    }
}
