package borja.lab1.part1;

public class SimpleThread extends Thread {

    private int id;

    public SimpleThread(int id) {
        super();
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("Thread [" + id + "]: I'm live!");

        try {
            sleep(Main.T);
        } catch (InterruptedException e) {
            System.out.println("Thread [" + id + "]: I don't wanna sleep");
        }

        System.out.println("Thread [" + id + "]: I'm out.");

    }
}
