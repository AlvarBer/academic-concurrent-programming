package borja.lab1.part2;



public abstract class Operator extends Thread {

    protected int id;

    public Operator(int id) {
        super();
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("Thread [" + id + "]: I'm live!");

        for (int i = 0; i < Main.N; i++) {
            Main.n = this.operation();
        }

        System.out.println("Thread [" + id + "]: I'm out. N = " + Main.n);

    }

    protected abstract int operation();

}
