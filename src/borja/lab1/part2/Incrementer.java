package borja.lab1.part2;

public class Incrementer extends Operator {
    public Incrementer(int id) {
        super(id);
    }

    @Override
    protected int operation() {
        return Main.n + 1;
    }
}
