package borja.lab1.part2;

public class Decrementer extends Operator {


    public Decrementer(int id) {
        super(id);
    }

    @Override
    protected int operation() {
        return Main.n - 1;
    }
}
