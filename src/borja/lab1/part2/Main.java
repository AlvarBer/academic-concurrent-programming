package borja.lab1.part2;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final int N = 3;
    public static final int M = 200;

    public static int n = 0;

    public static void main(String[] args) throws InterruptedException {

        List<Operator> threads = new ArrayList<>();

        for (int i = 0; i < M; i++) {
            threads.add(new Incrementer(i));
            threads.add(new Decrementer(i + M));
        }

        for (Thread t : threads) {
            t.start();
        }

        for (Thread t : threads) {
            t.join();
        }

        System.out.println("\nAll said and done, the value of N is " + Main.n);

    }
}
